import React from "react";
import { Container } from "reactstrap";

// import VotersTable from "./components/VotersTable";
import Users from "./components/Users";
import CreateLink from "./components/CreateLink";

const App = () => (
  <Container>
    <Users />
    <CreateLink />
  </Container>
);

export default App;
