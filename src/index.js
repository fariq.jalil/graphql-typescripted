import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { ApolloProvider } from "react-apollo";
import { ApolloClient } from "apollo-client";
import { createHttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";

// Create the httpLink that will connect your
// ApolloClient instance with the GraphQL API,
// your GraphQL server will be running on http://localhost:4000.
const httpLink = createHttpLink({
  uri: "https://fakerql.com/graphql"
  // uri: "http://localhost:4000"
});

// Instantiate ApolloClient by passing in the httpLink
// and a new instance of an InMemoryCache.
export const client = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache()
});

// The App is wrapped with the higher-order component ApolloProvider
// that gets passed the client as a prop
const RenderredApp = () => (
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>
);

ReactDOM.render(<RenderredApp />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
