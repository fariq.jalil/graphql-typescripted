import React from "react";
import { Card, CardBody } from "reactstrap";

import Todos from "./Todos";

Todos;

type UserType = {
  firstName: string;
  lastName: string;
  avatar: string;
};

export type PropsType = {
  user: UserType;
};

const UsersData = ({ user }: PropsType) => {
  const { firstName, lastName, avatar } = user;

  const cardStyle = {
    minHeight: "200px",
    marginBottom: "20px",
    boxShadow: "3px 3px #ba8b00"
  };

  const imgStyle = { borderRadius: "30px", border: "1px solid #ffc107" };

  return (
    <Card outline color="warning" style={{ ...cardStyle, textAlign: "center" }}>
      <CardBody>
        <img style={imgStyle} src={avatar} width="60px" />
        <br />
        <br />
        <h6>
          {firstName} {lastName}
        </h6>
        <Todos />
      </CardBody>
    </Card>
  );
};

export default UsersData;
