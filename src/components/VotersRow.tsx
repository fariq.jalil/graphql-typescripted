import React from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";
import VotersData from "./VotersData";

export const FEED_QUERY = gql`
  {
    feed {
      links {
        id
        createdAt
        url
        description
      }
    }
  }
`;

const VotersRow = () => (
  <Query query={FEED_QUERY}>
    {({ loading, error, data }) => {
      if (error) {
        return (
          <tr>
            <td colSpan={4}>Something went wrong!!!</td>
          </tr>
        );
      }

      return (
        <>
          {loading && (
            <tr>
              <td colSpan={4}>loading...</td>
            </tr>
          )}

          {data.feed &&
            (data.feed.links.length > 0 ? (
              data.feed.links.map((link: any, idx: number) => (
                <VotersData index={idx} key={link.id} link={link} />
              ))
            ) : (
              <tr>
                <td colSpan={4}>No record found...</td>
              </tr>
            ))}
        </>
      );
    }}
  </Query>
);

export default VotersRow;
