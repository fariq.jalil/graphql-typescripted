import gql from "graphql-tag";

export const ALLTODOS = gql`
  {
    allTodos(count: 2) {
      title
      completed
    }
  }
`;
