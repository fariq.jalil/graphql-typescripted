import gql from "graphql-tag";

export const ALLUSERS = gql`
  {
    allUsers(count: 9) {
      id
      firstName
      lastName
      email
      avatar
    }
  }
`;
