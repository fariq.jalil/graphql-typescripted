import React from "react";
import { AssignmentTurnedIn, Assignment } from "styled-icons/material";

type TodoType = {
  title: string;
  completed: boolean;
};

export type PropsType = {
  index: number;
  todo: TodoType;
};

const Todo = ({ index, todo }: PropsType) => {
  const { title, completed } = todo;

  return (
    <li>
      {title}{" "}
      {completed ? (
        <AssignmentTurnedIn style={{ color: "green" }} size={20} />
      ) : (
        <Assignment size={20} />
      )}
    </li>
  );
};

export default Todo;
