import React from "react";

type PostedByType = {
  name?: string;
};

type LinkType = {
  description: string;
  url: string;
  postedBy?: PostedByType;
  votes?: [];
};

export type PropsType = {
  index: number;
  link: LinkType;
};

const VotersData = ({ index, link }: PropsType) => (
  <tr>
    <td>{index + 1}</td>
    <td>
      <a href={link.url} target="_blank">
        {link.description}
      </a>
    </td>
    <td>{link.postedBy ? link.postedBy.name : "unknown"}</td>
    <td>{link.votes ? link.votes.length : "no record"}</td>
  </tr>
);

export default VotersData;
