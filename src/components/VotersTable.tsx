import React from "react";
import { Card, CardBody, Table } from "reactstrap";

import VotersRow from "./VotersRow";

const VotersTable = () => (
  <Card>
    <CardBody>
      <Table hover responsive>
        <thead>
          <tr>
            <th>No.</th>
            <th>Link</th>
            <th>Posted By</th>
            <th>Votes</th>
          </tr>
        </thead>
        <tbody>
          <VotersRow />
        </tbody>
      </Table>
    </CardBody>
  </Card>
);

export default VotersTable;
