import React, { useState } from "react";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import { Button, FormGroup, Label, Input, Row, Col } from "reactstrap";

import { FEED_QUERY } from "./VotersRow";

const POST_MUTATION = gql`
  mutation PostMutation($description: String!, $url: String!) {
    post(description: $description, url: $url) {
      id
      createdAt
      url
      description
    }
  }
`;

const CreateLink = () => {
  const [description, setDescription] = useState("");
  const [url, setUrl] = useState("");

  const rowStyle = {
    alignItems: "flex-end"
  };

  const btnMargin = {
    marginBottom: "18px"
  };

  return (
    <Row style={rowStyle}>
      <Col md="10" lg="11">
        <FormGroup>
          <Label for="description">Description</Label>
          <Input
            id="description"
            onChange={e => setDescription(e.target.value)}
            type="text"
            placeholder="A description for the link"
          />
        </FormGroup>
        <FormGroup>
          <Label for="url">URL</Label>
          <Input
            id="url"
            onChange={e => setUrl(e.target.value)}
            type="text"
            placeholder="The URL for the link"
          />
        </FormGroup>
      </Col>
      <Col
        style={{
          textAlign: "center"
        }}
        md="2"
        lg="1"
      >
        <Mutation
          mutation={POST_MUTATION}
          variables={{ description, url }}
          refetchQueries={[{ query: FEED_QUERY }]}
          awaitRefetchQueries={true}
        >
          {(postMutation: any, { loading, error }) => (
            <Button
              style={btnMargin}
              type="button"
              color="warning"
              onClick={postMutation}
              disabled={loading}
            >
              {loading ? "Adding..." : "Add"}
            </Button>
          )}
        </Mutation>
      </Col>
    </Row>
  );
};
export default CreateLink;
