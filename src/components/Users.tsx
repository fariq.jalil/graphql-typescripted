import React from "react";
import { Row, Col } from "reactstrap";
import { Query } from "react-apollo";

import { ALLUSERS } from "./queries/AllUsers";
import User from "./User";

const Users = () => (
  <Query query={ALLUSERS}>
    {({ loading, error, data }) => {
      if (error) {
        return <div>Something went wrong!!!</div>;
      }

      console.log(data);

      return (
        <>
          {loading && <div>loading...</div>}

          <Row>
            {data.allUsers &&
              (data.allUsers.length > 0 ? (
                data.allUsers.map((user: any, idx: number) => (
                  <Col key={user.id} md="4">
                    <User user={user} />
                  </Col>
                ))
              ) : (
                <div>No record found...</div>
              ))}
          </Row>
        </>
      );
    }}
  </Query>
);

export default Users;
