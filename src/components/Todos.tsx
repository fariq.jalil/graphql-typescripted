import React from "react";
import { Row, Col } from "reactstrap";
import { Query } from "react-apollo";

import { ALLTODOS } from "./queries/allTodos";
import Todo from "./Todo";

const Todos = () => (
  <Query query={ALLTODOS}>
    {({ loading, error, data }) => {
      if (error) {
        return <div>Something went wrong!!!</div>;
      }

      console.log(data);

      return (
        <div style={{ textAlign: "left" }}>
          <p>Assignment</p>
          {loading && <div>loading...</div>}
          <ul>
            {data.allTodos &&
              (data.allTodos.length > 0 ? (
                data.allTodos.map((todo: any, idx: number) => (
                  <Todo key={idx} index={idx} todo={todo} />
                ))
              ) : (
                <li>Yeay. No task for this week...</li>
              ))}
          </ul>
        </div>
      );
    }}
  </Query>
);

export default Todos;
